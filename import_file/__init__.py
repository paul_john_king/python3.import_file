__author__ = "Paul John King"
__email__ = "paul_john_king@web.de"
__uri__ = "https://gitlab.com/paul_john_king/python3.mddoc"
__status__ = "Development"
__copyright__ = "© 2019 Paul John King (paul_john_king@web.de)"
__license__ = """\
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.\
"""
__doc__ = f"""\
A Python 3 module that provides a module-import function.\
"""

from importlib.util import spec_from_loader, module_from_spec
from importlib.machinery import SourceFileLoader
from os.path import abspath
from sys import modules
from types import ModuleType

def import_file(path: str, name: str) -> ModuleType:
	"""\
Import a module from a file, even if the file path has no suffix.

Args:
*   `path`(`str`): A path to a file containing a module.
*   `name`(`str`): The name to give to the module.

Returns:
*   A module with the name `name` built from the contents of the file at
    the path `path`.
"""
	loader = SourceFileLoader(name, path)
	spec = spec_from_loader(name, loader)
	module = module_from_spec(spec)
	spec.loader.exec_module(module)
	modules[name] = module
	return module
