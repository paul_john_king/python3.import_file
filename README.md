© 2019 Paul John King (paul_john_king@web.de)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.

Note
====

`readme` generates this project's `README.md` file from documentation within
the project's source code.  In order to modify the contents of the `README.md`
file, edit the source code documentation, set the current working directory to
the root of the project's directory tree, and call `readme`.

Package
=======

Name
----

`import_file` - A Python 3 module that provides a module-import function.

Package Contents
----------------



Functions
---------

*   `import_file(path:str, name:str) -> module` -- Import a module from a file, even if the file path has no suffix.
    
    Args:
    *   `path`(`str`): A path to a file containing a module.
    *   `name`(`str`): The name to give to the module.
    
    Returns:
    *   A module with the name `name` built from the contents of the file at
        the path `path`.

Data
----

*   `__copyright__ = '© 2019 Paul John King (paul_john_king@web.de)'`
*   `__email__ = 'paul_john_king@web.de'`
*   `__license__ = 'This program is free software: you can redistrib...ram....`
*   `__status__ = 'Development'`
*   `__uri__ = 'https://gitlab.com/paul_john_king/python3.mddoc'`
*   `modules = {'__main__': <module '__main__' from './readme'>, '_ast': <m...`

Author
------

Paul John King

File
----

`/home/paul/projects/gitlab.com/paul_john_king/python3.import_file/import_file/__init__.py`

